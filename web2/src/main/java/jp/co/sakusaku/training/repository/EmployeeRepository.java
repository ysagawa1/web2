package jp.co.sakusaku.training.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jp.co.sakusaku.training.entity.Employee;



@Repository

public interface EmployeeRepository extends JpaRepository<Employee, Long> {}
