//フォーム（入力画面）⇒アドレスリスト（結果表示）
package jp.co.sakusaku.training.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.entity.Task;
import jp.co.sakusaku.training.form.TaskForm;
import jp.co.sakusaku.training.repository.TaskRepository;

//1-5演習
@Controller
public class TaskForm2Controller {

	@Autowired
	TaskRepository taskRepository;

	//入力画面　URL 指定　GETメソッド
	@RequestMapping(value = "/taskform2", method = RequestMethod.GET)
	public String index(Model model) {
		
		
		//インスタンス化
		TaskForm form = new TaskForm();
		//現在の日にち
		form.setLimitDate(LocalDate.now());
		model.addAttribute("taskForm", form);

		return "taskform2";
	}

	//結果画面(リスト)　URL指定　POSTメソッド
	@RequestMapping(value = "/taskform2/submit", method = RequestMethod.POST)
	//@Validated 入力チェック　BindingResultにエラーを格納
	public String submit(Model model, @Validated TaskForm taskForm, BindingResult result) {

		model.addAttribute("taskForm", taskForm);

		//エラーの場合は画面移動しない
		if (result.hasErrors()) {

			return "taskform2";
		} else {

			//エラー以外はsubmit(リスト画面)へ移動
			Task task = new Task();
			task.setTitle(taskForm.getTitle());
			task.setDetail(taskForm.getDetail());
			task.setLimitDate(taskForm.getLimitDate());
			taskRepository.insert(task);

			//taskList の全てを検索範囲
			List<Task> taskList = taskRepository.findAll();
			model.addAttribute("tasklist", taskList);

			return "taskform2_submit";
		}
	}
}