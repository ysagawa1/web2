package jp.co.sakusaku.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.entity.Address;
import jp.co.sakusaku.training.form.AddressForm;
import jp.co.sakusaku.training.repository.AddressRepository;

//import

//1-5課題
@Controller
public class AddressForm2Controller {
	
	@Autowired
	AddressRepository addressRepository;
	
	//入力画面　URL指定　GETメソッド   index=索引
	@RequestMapping (value= "/addressform2",method=RequestMethod.GET)
	public String index(Model  model) {
		
		//インスタンス化
		AddressForm form = new AddressForm();
		model.addAttribute("addressForm",form);
		
		return "addressform2";
		
	}
	
	//結果画面　URL指定　POSTメソッド  
	@RequestMapping(value="/addressform2/submit",method=RequestMethod.POST)
	public String submit(Model model,@Validated AddressForm addressForm,BindingResult result) {
		
		model.addAttribute("addressForm",addressForm);
		
		//もしエラーの場合addressform2にとどまる
		if(result.hasErrors()) {
			return"addressform2";
		}else {
			//インスタンス化
			Address address =new Address();
		    address.setPrmLastName(addressForm.getPrmLastName());
		    address.setPrmFirstName(addressForm.getPrmFirstName());
		    address.setPhoneNumber(addressForm.getPhoneNumber());
			address.setPrmAddress(addressForm.getPrmAddress());
			address.setBirthDay(addressForm.getBirthDay());
		    
			//insert文　デーブルにデータ登録　★
		    addressRepository.insert(address);
		    
		    List<Address>addresForm2 = addressRepository.findAll();
		    model.addAttribute("addressForm2",addresForm2);
		    
		    return"addressform2_submit";
		    
		}
	}
}
