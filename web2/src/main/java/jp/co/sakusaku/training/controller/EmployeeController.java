package jp.co.sakusaku.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.entity.Employee;
import jp.co.sakusaku.training.repository.EmployeeRepository;

@Controller

public class EmployeeController {

	@Autowired

	EmployeeRepository empRepository;

	@RequestMapping(value = "/employee", method = RequestMethod.GET)

	public String index(Model model) {

		List<Employee> emplist = empRepository.findAll();

		model.addAttribute("emplist", emplist);

		return "employee";

	}

}
