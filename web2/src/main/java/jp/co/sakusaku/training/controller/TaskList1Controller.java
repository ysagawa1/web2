package jp.co.sakusaku.training.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
//属性を追加
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.entity.Task;

@Controller
public class TaskList1Controller {

	//URLを指定、value省略可能
	@RequestMapping(value = "/tasklist1", method = RequestMethod.GET)
	//index 見つからない場合エラーを返す
	public String index(Model model) {

		//インスタンス化
		List<Task> taskList = new ArrayList<Task>();

		//0～4までくり返し 
		for (int i = 0; i < 5; i++) {

			//カラムの作成
			Task task1 = new Task();

			//Lを付けるのはLong型を見分けるため
			task1.setId(i + 1l);
			task1.setTitle("タイトル" + (i + 1));
			task1.setDetail("詳細" + (i + 1));

			//現在から日にちに＋１
			LocalDate d = LocalDate.now();
			d = d.plusDays(i);
			task1.setLimitDate(d);
			taskList.add(task1);
		}
		//名前と値を指定
		model.addAttribute("tasklist", taskList);

		return "tasklist1";
	}
}
