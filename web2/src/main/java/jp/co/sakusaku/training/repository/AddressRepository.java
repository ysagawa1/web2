package jp.co.sakusaku.training.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import jp.co.sakusaku.training.entity.Address;

//1-6 課題
@Repository
public class AddressRepository {

	//JDBCの新しいテンプレート作成
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	//findAllを利用
	public List<Address> findAll() {

		//query H2に問い合わせ（アドレス一覧のカラムの情報）
		return jdbcTemplate.query("select *from address",
				new BeanPropertyRowMapper<Address>(Address.class));
	}

	// 姓、名、住所　絞り込み
	public List<Address> findAll(String prmLastName, String prmFirstName, String prmAddress) {

		//Like句でカラムに対して検索　★
		//SQLへ
		String selectSql = "select * from Address";
		String whereSql = "";

		//条件：姓がnullか空でない場合
		//処理：姓の検索条件のSQLをwhereSqlに追加

		if (!(prmLastName.isBlank())) {
			whereSql = " where prmLastName like :prmLastName%";
		}
		//条件1：名がnullか空でない場合
		if (!(prmFirstName.isBlank())) {
			whereSql = " where prmFirstName like :prmFirstName%";
		}
		//条件1：住所がnullか空でない場合 
		if (!(prmAddress.isBlank())) {
			whereSql = " where prmAddress like :prmAddress%";
		}
		//条件2-1：whereSqlが空の場合 全空白
		//処理：where 名（SQL文）
		if (whereSql == "") {
			whereSql = " where prmFirstName like :prmFirstName%";

		//条件2-2：それ以外  2か所記入
		//処理：AND　名
		} else if (!(prmFirstName.isBlank() && prmLastName.isBlank())) {
			whereSql = " where prmFirstName like :prmFirstName and prmLastName like :prmLastName";
		} else if (!(prmFirstName.isBlank() && prmAddress.isBlank())) {
			whereSql = " where prmFirstName like :prmFirstName and prmAddress like :prmAddress";
		} else if (!(prmLastName.isBlank() && prmAddress.isBlank())) {
			whereSql = " where prmLastName like :prmLastName and prmAddress like :prmAddress";
		}
		selectSql += whereSql;

		//	//多田隈さんの解答例
		//		if (!prmLastName.isBlank()) {​
		//			whereSql = "where prmLastName like :prmLastName% ";
		//			}
		//
		//			if (!prmFirstName.isBlank()) {​
		//			if (whereSql.isBlank()) {​
		//			whereSql = "where prmFirstName like :prmFirstName% ";
		//			} else {​
		//			whereSql = "and prmFirstName like :prmFirstName% ";
		//			}
		//			}
		//
		//			if (!prmAddress.isBlank()) {​
		//			if(whereSql.isBlank()) {​
		//			whereSql = "where prmAddress like :prmAddress% ";
		//			} else {​
		//			whereSql = "and prmAddress like :prmAddress% ";
		//			}
		//			}

		//インスタンス化
		Address address = new Address();
		//検索
		address.setPrmLastName(prmLastName + "%");
		address.setPrmFirstName(prmFirstName + "%");
		address.setPrmAddress(prmAddress + "%");

		SqlParameterSource paramMap = new BeanPropertySqlParameterSource(address);

		return jdbcTemplate.query(selectSql, paramMap, new BeanPropertyRowMapper<Address>(Address.class));
	}

	public int insert(Address address) {
		//SQLへinsert文（登録）の命令　(列名)values(値)  
		final String insertSql = "insert into address (id,prmLastName,prmFirstName,phoneNumber,prmAddress,birthDay  )"
				+ "values(:prmLastName,:prmFirstName,:phoneNumber,:pamAddress,:birthDay)";

		//例外処理
		try {
			SqlParameterSource param = new BeanPropertySqlParameterSource(address);
			return jdbcTemplate.update(insertSql, param);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
