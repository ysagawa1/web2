package jp.co.sakusaku.training.controller;
//import

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.entity.Address;
import jp.co.sakusaku.training.repository.AddressRepository;

@Controller
public class AddressList2Controller {

	@Autowired
	AddressRepository addressRepository;

	//URLの指定　GETメソッド
	@RequestMapping(value = "/addresslist2", method = RequestMethod.GET)

	//index エラーを返す
	public String index(Model model) {

		//Repositoryからすべての情報を検索対象とする

		List<Address> addressList = addressRepository.findAll();
		model.addAttribute("addresslist", addressList);

		return "addresslist2";

	}
}
