package jp.co.sakusaku.training.controller;

//important
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.entity.Task;
import jp.co.sakusaku.training.repository.TaskRepository;

//1－4演習
@Controller
public class TaskList2Controller {

	//管理下のオブジェクトから適切なものを変数に自動セット
	@Autowired
	TaskRepository taskRepository;

	//URLの指定　GETメソッド　入力データの受取
	@RequestMapping(value = "/tasklist2", method = RequestMethod.GET)
	public String index(Model model) {

		//Repositoryクラスから全ての情報を検索対象とする
		List<Task> taskList = taskRepository.findAll();
		model.addAttribute("tasklist", taskList);

		return "tasklist2";
	}
}
