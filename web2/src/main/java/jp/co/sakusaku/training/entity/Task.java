package jp.co.sakusaku.training.entity;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;


public class Task {

	//フィールド
	private Long id;
	private String title;
	private String detail;
	private LocalDate limitDate;

	private String TaskSearch;
	

	//行検索　アクセサ
	public String getTaskSearch() {
		return TaskSearch;
	}

	public void setTaskSearch(String taskSearch) {
		TaskSearch = taskSearch;
	}

	//アクセサメソッド
	//データベースからセッターへ
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public LocalDate getLimitDate() {
		return limitDate;
	}

	public void setLimitDate(LocalDate limitDate) {
		this.limitDate = limitDate;
	}
	
	//DateクラスでlimitDateを返却する yyyy/MM/ddで表示するため
	public Date getLimitDate2() {
		Calendar cal = Calendar.getInstance();
		
		
		//西暦、月、日を呼び出し
		cal.set(limitDate.getYear(), limitDate.getMonthValue() - 1,
				limitDate.getDayOfMonth());
		//秒数まで呼び出し　Date型で変数dに代入
		Date d = new Date(cal.getTimeInMillis());
		return d;
	}
}
