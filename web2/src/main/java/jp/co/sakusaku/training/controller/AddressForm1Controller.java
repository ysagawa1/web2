package jp.co.sakusaku.training.controller;

//import
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.form.AddressForm;

//メイン
@Controller
public class AddressForm1Controller {

	//URL指定　GETメソッドでデータ受け取り　★
	@RequestMapping(value = "/addressform1", method = RequestMethod.GET)

	//index エラーを返す
	public String index(Model model) {

		//インスタンス化(忘れがち) まずは実体を作る
		AddressForm form = new AddressForm();

		//入力フォーム
		model.addAttribute("addressForm", form);
		//戻り値
		return "addressform1";
	}

	//サブ　POSTメソッドでデータ送信　★
	@RequestMapping(value = "/addressform1/submit", method = RequestMethod.POST)

	//入力されて送られたデータをサブ画面で表示　★
	public String submit(Model model, @Validated AddressForm addressForm, BindingResult result) {

		model.addAttribute("addressForm", addressForm);
		//もし空白（エラー）ならそのまま
		if (result.hasErrors()) {

			return "addressform1";
			//空白（エラー）以外の場合はsubmitへ
		} else {
			return "addressform1_submit";

		}
	}
}
