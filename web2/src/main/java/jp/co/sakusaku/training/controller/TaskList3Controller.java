package jp.co.sakusaku.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.entity.Task;
import jp.co.sakusaku.training.form.TaskForm;
import jp.co.sakusaku.training.repository.TaskRepository;

//1-6演習
@Controller
public class TaskList3Controller {
	@Autowired
	TaskRepository taskRepository;

	//検索画面　URL指定　GETメソッド タイトル検索ページ移動なし
	@RequestMapping(value = "/tasklist3", method = RequestMethod.GET)
	public String index(Model model) {

		//入力欄
		TaskForm form = new TaskForm();
		model.addAttribute("taskForm", form);

		//一覧 ArrayListにデータを詰める ★
		List<Task> taskList = taskRepository.findAll();
		model.addAttribute("tasklist", taskList);

		return "tasklist3";
	}

	//結果画面 URL指定 POSTメソッド タイトル検索結果 ★
	@RequestMapping(value = "/tasklist3/submit", method = RequestMethod.POST)
	public String submit(Model model, @Validated TaskForm taskForm, BindingResult result) {

		model.addAttribute("taskForm", taskForm);
		List<Task> taskList = taskRepository.findByTitle(taskForm.getTitle());
		model.addAttribute("tasklist", taskList);

		return "tasklist3";
	}
}
