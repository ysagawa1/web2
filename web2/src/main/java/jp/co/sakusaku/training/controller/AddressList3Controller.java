package jp.co.sakusaku.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.entity.Address;
import jp.co.sakusaku.training.form.AddressForm;
import jp.co.sakusaku.training.repository.AddressRepository;

//1-6課題
@Controller
public class AddressList3Controller {
	@Autowired
	AddressRepository addressRepository;

	//検索画面
	@RequestMapping(value = "/addresslist3", method = RequestMethod.GET)
	public String index(Model model) {

		//入力欄 
		AddressForm form = new AddressForm();
		model.addAttribute("addressForm", form);

		//一覧
		List<Address> addressList = addressRepository.findAll();
		model.addAttribute("addresslist", addressList);

		return "addresslist3";
	}

	//結果画面　URL指定  アドレス検索結果
	@RequestMapping(value = "/addresslist3/submit", method = RequestMethod.POST)
	public String submit(Model model, @Validated AddressForm addressForm, BindingResult result) {

		//絞り込み検索　姓、名、住所
		model.addAttribute("addressform", addressForm);
		List<Address> addressList = addressRepository.findAll(addressForm.getPrmLastName(),
				addressForm.getPrmFirstName(), addressForm.getPrmAddress());

		model.addAttribute("addresslist", addressList);

		return "addresslist3";

	}
}