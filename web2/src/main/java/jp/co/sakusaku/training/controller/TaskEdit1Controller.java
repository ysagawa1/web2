//フォーム（編集画面）
package jp.co.sakusaku.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jp.co.sakusaku.training.entity.Task;
import jp.co.sakusaku.training.form.TaskForm;
import jp.co.sakusaku.training.repository.TaskRepository;

//1-7演習
@Controller
public class TaskEdit1Controller {

	@Autowired
	TaskRepository taskRepository;

	//入力画面　URL 指定　GETメソッド
	@RequestMapping(value = "/taskedit1", method = RequestMethod.GET)
	public String index(Model model, @RequestParam(name = "id", required = false) Long id) {

		//入力フォーム 初期値  　
		//setはForm　HTMLはformなのでTaskformにする

		Task taskForm = new Task();
		taskForm = taskRepository.findById(id);

		taskForm.setId(taskForm.getId());
		taskForm.setTitle(taskForm.getTitle());
		taskForm.setDetail(taskForm.getDetail());
		taskForm.setLimitDate(taskForm.getLimitDate());

		model.addAttribute("taskForm", taskForm);

		return "taskedit1";
	}

	//結果画面(リスト)　URL指定　POSTメソッド  更新
	@RequestMapping(value = "/tasklist4", params = "update", method = RequestMethod.POST)
	public String update(Model model, @Validated TaskForm taskForm, BindingResult result) {

		//エラーの場合は画面移動しない
		if (result.hasErrors()) {
			return "taskedit1";
		} else {

			//エラー以外はtaskList4(リスト画面)へ移動
			Task task = new Task();

			task.setId(taskForm.getId());
			task.setTitle(taskForm.getTitle());
			task.setDetail(taskForm.getDetail());
			task.setLimitDate(taskForm.getLimitDate());
			taskRepository.update(task);

			//taskList の全てを検索範囲
			List<Task> taskList = taskRepository.findAll();
			model.addAttribute("tasklist", taskList);

			//TaskListのタイトル検索フォームに値が入らない様に空のフォームを用意する
			taskForm = new TaskForm();
			model.addAttribute("taskForm", taskForm);

			return "tasklist4";
		}
	}

	//結果画面（リスト）URL指定　削除
	@RequestMapping(value = "/tasklist4", params = "delete", method = RequestMethod.POST)
	public String delete(Model model, @Validated TaskForm taskForm, BindingResult result) {

		if (result.hasErrors()) {
			return "taskedit1";

		} else {
			Task task = new Task();

			task.setId(taskForm.getId());
			task.setTitle(taskForm.getTitle());
			task.setDetail(taskForm.getDetail());
			task.setLimitDate(taskForm.getLimitDate());
			taskRepository.delete(task);

			List<Task> taskList = taskRepository.findAll();
			model.addAttribute("tasklist", taskList);

			taskForm = new TaskForm();
			model.addAttribute("taskForm", taskForm);

			return "tasklist4";
		}
	}
}