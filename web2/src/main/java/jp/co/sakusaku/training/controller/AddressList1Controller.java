package jp.co.sakusaku.training.controller;

//import
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.entity.Address;

@Controller
public class AddressList1Controller {

	//URL指定
	@RequestMapping(value = "/addresslist1", method = RequestMethod.GET)
	//index  エラーを返す
	public String index(Model model) {
		//インスタンス化
		List<Address> addressList = new ArrayList<>();

		//カラムの制作
		Address address = new Address();
		address.setPrmLastName("【姓】");
		address.setPrmFirstName("【名】");
		address.setPhoneNumber("【電話番号】");
		address.setPrmAddress("【住所】");
		//address.setBirthDay("【誕生日】");

		addressList.add(address);

		//id (それぞれにデータを詰め込む)
		int i = 0;

		//一人目 インスタンス化（データを詰め込む）
		Address address1 = new Address();
		address1.setId(i + 1l);
		address1.setPrmLastName("清水");
		address1.setPrmFirstName("穂香");
		address1.setPhoneNumber("0743339166");
		address1.setPrmAddress("京都府");
		//address1.setBirthDay(1963/02/15);

		addressList.add(address1);

		//二人目　
		Address address2 = new Address();
		address2.setId(i + 2l);
		address2.setPrmLastName("江成");
		address2.setPrmFirstName("勝彦");
		address2.setPhoneNumber("0591574862");
		address2.setPrmAddress("岐阜県");
		//address2.setBirthDay(1961/01/24);

		addressList.add(address2);

		//名前と値の指定
		model.addAttribute("addresslist", addressList);

		return "addresslist1";

	}
}
