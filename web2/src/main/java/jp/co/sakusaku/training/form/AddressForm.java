package jp.co.sakusaku.training.form;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import jdk.jfr.Name;

public class AddressForm {

	//フィールド
	private Long id;

	//空白をエラーにする
	@NotBlank

	//入力文字数
	@Size(min = 0, max = 10)
	@Name(value = "姓")
	private String prmLastName;

	@Size(min = 9, max = 10)
	@Name(value = "名")
	private String prmFirstName;

	@Size(min = 0, max = 17)
	@Name(value = "電話番号")
	private String phoneNumber;

	@Size(min = 0, max = 10)
	@Name(value = "住所")
	private String prmAddress;

	//日付(表示パターンを指定)
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	@PastOrPresent
	private LocalDate birthDay;

	

	//アクセサメソッド

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrmLastName() {
		return prmLastName;
	}

	public void setPrmLastName(String prmLastName) {
		this.prmLastName = prmLastName;
	}

	public String getPrmFirstName() {
		return prmFirstName;
	}

	public void setPrmFirstName(String PrmFirstName) {
		this.prmFirstName = PrmFirstName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPrmAddress() {
		return prmAddress;
	}

	public void setPrmAddress(String prmAddress) {
		this.prmAddress = prmAddress;
	}

	public LocalDate getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(LocalDate birthDay) {
		this.birthDay = birthDay;
	}

}
