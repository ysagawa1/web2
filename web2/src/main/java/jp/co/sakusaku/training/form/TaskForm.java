package jp.co.sakusaku.training.form;

import java.time.LocalDate;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import jdk.jfr.Name;

public class TaskForm {

	private Long id;

	//空白NG
	@NotBlank
	@Size(min = 0, max = 32)
	@Name(value = "タイトル")
	private String title;

	@Size(min = 0, max = 100)
	private String detail;

	//springでのDate表示形式、指定
	@DateTimeFormat(pattern ="yyyy/MM/dd")
	//現在より未来のみ出力
	@FutureOrPresent
	private LocalDate limitDate;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public LocalDate getLimitDate() {
		return limitDate;
	}

	public void setLimitDate(LocalDate limitDate) {
		this.limitDate = limitDate;
	}
}
