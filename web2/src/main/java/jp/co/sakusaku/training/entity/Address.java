package jp.co.sakusaku.training.entity;

//import
import java.time.LocalDate;

public class Address {

	//フィールド
	private Long id;
	private String prmLastName;
	private String prmFirstName;
	private String phoneNumber;
	private String prmAddress;
	//LocalDateは時間管理なし
	private LocalDate birthDay;

	private String addressSearch;

	//タイトル検索
	public String getAddressSearch() {
		return addressSearch;
	}

	public void setAddressSearch(String addressSearch) {
		this.addressSearch = addressSearch;
	}

	//アクセサメソッド
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrmLastName() {
		return prmLastName;
	}

	public void setPrmLastName(String prmLastName) {
		this.prmLastName = prmLastName;
	}

	public String getPrmFirstName() {
		return prmFirstName;
	}

	public void setPrmFirstName(String prmFirstName) {
		this.prmFirstName = prmFirstName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPrmAddress() {
		return prmAddress;
	}

	public void setPrmAddress(String prmAddress) {
		this.prmAddress = prmAddress;
	}

	public LocalDate getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(LocalDate birthDay) {
		this.birthDay = birthDay;
	}
}