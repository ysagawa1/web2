package jp.co.sakusaku.training.controller;

import java.time.LocalDate;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.sakusaku.training.form.TaskForm;

//メイン
@Controller
public class TaskForm1Controller {

	//入力画面　URL指定　GETメソッドでデータ受け取り　
	@RequestMapping(value = "/taskform1", method = RequestMethod.GET)
	//エラーを返す	
	public String index(Model model) {

		//入力フォーム
		TaskForm form = new TaskForm();
		//現在日時を表示
		form.setLimitDate(LocalDate.now());
		model.addAttribute("taskForm", form);

		return "taskform1";
	}

	//結果画面　URL指定 POSTメソッドでデータ送信
	@RequestMapping(value = "/taskform1/submit", method = RequestMethod.POST)

	//結果画面で送られた入力データを表示、@Validated入力チェック、BindingResultにエラーを格納
	public String submit(Model model, @Validated TaskForm taskForm, BindingResult result) {

		model.addAttribute("taskForm", taskForm);

		//指定した形式以外での入力（もしくは空白）でのエラーの場合はtaskform1のままで
		if (result.hasErrors()) {
			return "taskform1";

		} else {
			return "taskform1_submit";
		}
	}
}
