package jp.co.sakusaku.training.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import jp.co.sakusaku.training.entity.Task;

//1－7演習
@Repository
public class TaskRepository {

	//JDBCTemplateの新しいTemplateを作成する
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	//findAllを利用
	public List<Task> findAll() {

		//query　データベースシステムH2に対する問い合わせ　
		return jdbcTemplate.query("select id,title,detail,limit_date from task",

				//BeanPropertyRowMapper　データ取得時に独自処理を入れる
				new BeanPropertyRowMapper<Task>(Task.class));
	}

	//タイトル検索　SELECT文 WHERE文

	public List<Task> findByTitle(String title) {

		String selectSql = "select id, title, detail, limit_date from task"
				+ " where title like :title";

		if (title == null || title.isBlank()) {
			return new ArrayList<Task>();
		}

		//インスタンス化
		Task task = new Task();
		//検索
		task.setTitle(title + "%");

		SqlParameterSource paramMap = new BeanPropertySqlParameterSource(task);

		return jdbcTemplate.query(selectSql, paramMap, new BeanPropertyRowMapper<Task>(Task.class));
	}

	//INSERT文でFormに入力されたレコードをリストに追加
	public int insert(Task task) {

		//SQL文
		String insertSql = "INSERT INTO task (title,detail,limit_date)VALUES(:title, :detail, :limitDate)";

		//例外処理
		try {
			SqlParameterSource param = new BeanPropertySqlParameterSource(task);

			return jdbcTemplate.update(insertSql, param);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	//idをキーにタスク情報取得　初期値

	public Task findById(Long id) {

		//SQL文
		String displaySql = "SELECT * FROM task WHERE id = :id";

		//インスタンス化
		Task task = new Task();
		task.setId(id);
		//行検索
		SqlParameterSource mapper = new BeanPropertySqlParameterSource(task);

		List<Task> task1 = jdbcTemplate.query(displaySql, mapper, new BeanPropertyRowMapper<Task>(Task.class));

		//1行目だけをとる　Task型で
		return task1.get(0);
	}

	//UPDATE文 更新
	public int update(Task task) {

		//SQLにupdate文の命令 ※選択したidに合わせて		
		String updateSql = "UPDATE task SET title= :title,detail= :detail,limit_Date= :LimitDate WHERE id= :id";

		//例外処理
		try {
			SqlParameterSource param = new BeanPropertySqlParameterSource(task);

			return jdbcTemplate.update(updateSql, param);

		} catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	//DELETE文 削除
	public int delete(Task task) {

		//SQLにdelete文の命令 ※選択したidに合わせて		
		String deleteSql = "DELETE FROM task WHERE id= :id";

		//例外処理
		try {
			SqlParameterSource param = new BeanPropertySqlParameterSource(task);

			return jdbcTemplate.update(deleteSql, param);

		} catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
}